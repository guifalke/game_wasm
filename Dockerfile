#FROM rust:1.70.0-alpine as builder

#RUN apk update && apk add --no-cache \
#    make \
#    protobuf-dev \
#    cmake \
    #gcc \
#    g++ \
#    fontconfig-dev

#WORKDIR /usr/src/game_wasm
#COPY . .

#RUN cargo install wasm-pack
#RUN wasm-pack build --target web

FROM nginx

ENV SERVICE_URL=gateway
ENV PORT=80

RUN rm -rf ./usr/share/nginx/html/index.html
RUN rm -rf ./usr/share/nginx/html/50x.html
RUN rm -rf ./usr/share/nginx/html/pkg

COPY ./pkg ./usr/share/nginx/html/pkg
COPY ./index.html ./usr/share/nginx/html