docker rm game -f
docker rmi game -f
docker build . -t game
docker run --name game -d -p 40000:80 game
docker network connect dockerNetwork gateway
docker network connect dockerNetwork game
