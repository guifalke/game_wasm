use wasm_bindgen::prelude::*;
use wasm_bindgen_futures::spawn_local;
use web_sys::{
    Window,
    CanvasRenderingContext2d,
};

use regex::Regex;

mod start;
use crate::start::start_js::start;

mod map_module;
use crate::map_module::map_js::draw;

#[wasm_bindgen]
extern {
    #[wasm_bindgen(js_namespace = console)]
    fn log(s: &str);
}

#[wasm_bindgen]
pub fn version(){
    log("0.0.0.1");
}

#[wasm_bindgen]
pub fn create_map() -> Result<(), JsValue> {
    log("Start create_map.");
    let ctx = start();
    log("2d context created.");
    log("Define query string.");
    spawn_local(async {
        let _ = draw(ctx, 7).await;
    });
    return Ok(());
}