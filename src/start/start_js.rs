use wasm_bindgen::prelude::*;
use web_sys::{
    HtmlCanvasElement,
    Document,
    CanvasRenderingContext2d,
};

#[wasm_bindgen]
extern "C" {
    #[wasm_bindgen(js_namespace = console)]
    fn log(s: &str);
}

#[wasm_bindgen]
pub fn start() -> CanvasRenderingContext2d {
    log("Start start.");
    let document: Document = web_sys::window().unwrap().document().unwrap();
    log("Document created.");
    let canvas = document.get_element_by_id("canvas").unwrap();
    log("Canvas found.");
    let canvas: web_sys::HtmlCanvasElement = canvas
        .dyn_into::<HtmlCanvasElement>()
        .map_err(|_| ())
        .unwrap();
    log("Canvas parsed.");
    let context: CanvasRenderingContext2d = canvas
        .get_context("2d")
        .unwrap()
        .unwrap()
        .dyn_into::<CanvasRenderingContext2d>()
        .unwrap();
    log("Canvas context created.");    
    context.begin_path();
    log("Canvas begin context.");
    return context;
}