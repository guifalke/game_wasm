use crate::map_module::model::{
	MapInstance,
	MapRoom,
};

use crate::map_module::map_draw_js::DrawWrapper;

pub struct MapUser {
	load_map: MapInstance,
	screen_w: i32,
	screen_h: i32,
	grid_w: f64,
	grid_h: f64,
	room_counter: i32,
	rooms_to_build: Vec<MapRoom>,
	callback: DrawWrapper,
}

impl MapUser {
	pub async fn new (
		map: MapInstance,
		s_w: i32,
		s_h: i32,
		draw_wrapper: DrawWrapper
	) -> MapUser {
		let g_w = (s_w as f64 / (map.x_size + 1) as f64) as f64;
		let g_h = (s_h as f64 / (map.y_size + 1) as f64) as f64; 
		return MapUser{			
			screen_h: s_h,
			screen_w: s_w,
			load_map: map,
			grid_w: g_w,
			grid_h: g_h,
			room_counter: 0,
			rooms_to_build: Vec::new(),
			callback: draw_wrapper, 
		};
	}

	pub fn draw_mapuser(&mut self, number_room: i32) {
		self.callback.log("Start grid");

		self.callback.log(&format!("Expected rooms to be build {}", number_room));

		for y_map_index in 0..self.load_map.y_size {
			//self.callback.log(&format!("Creating y row for index {}.",y_map_index));
			self.draw_y_index_ruler(y_map_index);			
			for x_map_index in 0..self.load_map.x_size {
				//self.callback.log(&format!("Creating x collum for index {}.",x_map_index));
				self.draw_x_index_ruler(x_map_index);
				let room: Option<MapRoom> = self.test_position_has_room(y_map_index, x_map_index);
				match room {
					Some(r) => {
						self.rooms_to_build.push(r);
						self.callback.log(&format!("Room with key pushed queued to build: x:{}, y:{}", x_map_index, y_map_index));
					},
					None => {
						self.callback.log(&format!("No room with key pushed queued to build: x:{}, y:{}", x_map_index, y_map_index));
					}
				}
				if x_map_index == (self.load_map.x_size -1) {
					self.draw_x_end_row(y_map_index, x_map_index);
				} else {
					let key: String = format!("{},{}", x_map_index, y_map_index);
					if !self.rooms_to_build.is_empty() {
						self.draw_room(y_map_index, x_map_index);
						self.deal_end_room(
							y_map_index,
							x_map_index
						);
					} else {
						//print!("{:02}.", x_map_index);
						self.draw_x_row_normal(y_map_index, x_map_index);	
					}
				}
			}
		}
	}

	fn test_room_numbers(&self, number_room: i32) -> bool{
		return number_room > self.room_counter;
	}

	fn test_position_has_room(
		&mut self,
		y_map_index: i32,
		x_map_index: i32
	) -> Option<MapRoom> {
		let key: String = format!("{},{}", x_map_index, y_map_index);
		if self.load_map.map_room.contains_key(&key){
			self.callback.log(
				&format!("Found a place to a room on y- {}, x- {} with the key {}.",
					y_map_index,
					x_map_index,
					key
				)
			);
			return self.load_map.map_room.remove(&key);		   
		}
		return None;
	}

	fn increase_room(&mut self) {
		self.room_counter = self.room_counter + 1;
	}

	fn draw_room(
		&self,
		y_map_index: i32,
		x_map_index: i32
	)	
	{
		//self.callback.log("Start call draw room");
		let iter = self.rooms_to_build.len();
		self.callback.log(&format!("Total room to inspect for draw {}.", iter));
		let mut c = 0;
		while c < iter {
			self.callback.log(
				&format!("Index {} of {} on room to build.",
				c,
				iter)
			);
			let room = self.rooms_to_build.get(c).unwrap();	
			//self.callback.log(&format!("Drawing a room on y- {}, x- {}.",y_map_index, x_map_index));
			if room.is_end_of_room(y_map_index, x_map_index){
				//self.callback.log(&format!("End draw_room on y- {}, x- {} and index {}.",y_map_index, x_map_index, c));
				self.draw_x_row_room_end(y_map_index, x_map_index, room.kind.as_str());
			}

			if room.is_inside_room(y_map_index, x_map_index){
				//self.callback.log(&format!("Is inside room on y- {}, x- {} and index {}.",y_map_index, x_map_index, c));
				self.draw_x_row_room_inside(y_map_index, x_map_index, room.kind.as_str());
			} else {
				//self.callback.log(&format!("Is outside room on y- {}, x- {} and index {}.",y_map_index, x_map_index, c));
				self.draw_x_row_normal(y_map_index, x_map_index);
			}
			c += 1;
		}
	}

	fn deal_end_room(
		&mut self,
		y_map_index: i32,
		x_map_index: i32	
	)
	{
		let mut index_to_remove: Vec<usize> = Vec::new();
		let iter = self.rooms_to_build.len();
		self.callback.log(&format!("Total room to inspect for draw {}.", iter));
		let mut c = 0;
		while c < iter {
			self.callback.log(
				&format!("Index {} of {} on room to end.",
				c,
				iter)
			);
			if self.rooms_to_build.get(c).unwrap().is_end_of_room(y_map_index, x_map_index) {
				self.callback.log(&format!("Abdon a room on y- {}, x- {}.",y_map_index, x_map_index));
				index_to_remove.push(c);
				//print!("{:02}e", x_map_index);
				self.increase_room();
			}
			c += 1;
		}
		while let Some(remove) = index_to_remove.pop() {
			self.rooms_to_build.remove(remove);
		}
	}
	
	fn calc_y_position(&self, y_map_index: i32) -> f64 {
		return ((y_map_index as f64) * self.grid_h) as f64;
	}

	fn calc_x_position(&self, x_map_index: i32) -> f64 {
		return  ((x_map_index as f64) * self.grid_w) as f64;
	}

	fn draw_y_index_ruler(&self, y_map_index: i32){
		//callback.log(&format!("Drawing a ruler on y_index- {}, x- {}.",y_map_index, 0));
		self.callback.draw_square_inv(
			0.0,
			self.calc_y_position(y_map_index),
			self.grid_w as f64,
			self.grid_h as f64,
			&format!("{}",y_map_index)
		);
	}

	fn draw_x_index_ruler(&self, x_map_index: i32){
		//callback.log(&format!("Drawing a ruler on y_index- {}, x- {}.",0, x_map_index));
		self.callback.draw_square_inv(
			self.calc_x_position(x_map_index),
			0.0,
			self.grid_w as f64,
			self.grid_h as f64,
			&format!("{}",x_map_index)
		);
	}

	fn draw_x_end_row(&self, y_map_index: i32, x_map_index: i32){
		//callback.log(&format!("Drawing a x end row on y- {}, x- {}.",y_map_index, x_map_index));
		self.callback.draw_square(
			self.calc_x_position(x_map_index+1),
			self.calc_y_position(y_map_index+1),
			self.grid_w as f64,
			self.grid_h as f64,
			&format!("{}",x_map_index));
	}

	fn draw_x_row_normal(&self, y_map_index: i32, x_map_index: i32){
		//callback.log(&format!("Drawing a row normal on y- {}, x- {}.",y_map_index, x_map_index));
		self.callback.draw_square(
			self.calc_x_position(x_map_index+1),
			self.calc_y_position(y_map_index+1),
			self.grid_w as f64,
			self.grid_h as f64,
			&format!("{}",x_map_index));
	}

	fn draw_x_row_room_end(
		&self,
		y_map_index: i32,
		x_map_index: i32,
		kind: &str
	){
		//self.callback.log(&format!("Drawing a end room on y- {}, x- {}.",y_map_index, x_map_index));
		self.callback.draw_square_filled(
			self.calc_x_position(x_map_index+1),
			self.calc_y_position(y_map_index+1),
			self.grid_w as f64,
			self.grid_h as f64,
			&format!("{}",x_map_index),
			kind
		);
	}

	fn draw_x_row_room_inside(&self, y_map_index: i32, x_map_index: i32, kind: &str){
		//callback.log(&format!("Drawing a room on y- {}, x- {}.",y_map_index, x_map_index));
		self.callback.draw_square_filled(
			self.calc_x_position(x_map_index+1),
			self.calc_y_position(y_map_index+1),
			self.grid_w as f64,
			self.grid_h as f64,
			&format!("{}",x_map_index),
			kind
		);
	}

}

