use wasm_bindgen::prelude::*;

use web_sys::CanvasRenderingContext2d;

#[wasm_bindgen]
extern {
    #[wasm_bindgen(js_namespace = console)]
    fn log(s: &str);
}

#[wasm_bindgen]
pub struct DrawWrapper{
    ctx: CanvasRenderingContext2d,
}

#[wasm_bindgen]
impl DrawWrapper{
    #[wasm_bindgen(constructor)]
    pub fn new(val: CanvasRenderingContext2d) -> DrawWrapper {
        DrawWrapper { ctx: val }
    }

    #[wasm_bindgen]
    pub fn draw_square_inv(
        &self,
        x_position: f64,
        y_position: f64,
        w: f64,
        h: f64,
        text: &str
    ){
        /* log(&format!(
            "Call draw_square_inv x_position: {}, y_position: {}, w: {}, h: {}, text: {}, text_x: {}, text_y: {}",
            x_position,
            y_position,
            w,
            h,
            text,
            (x_position + (w/2.0)),
            (y_position + (h/2.0))
        )); */
        self.ctx.rect(
            x_position,
            y_position,
            w,
            h
        );

        self.text_in_rec(
            text,
            x_position + (w/2.0),
            y_position + (h/2.0)
        );
    }

    #[wasm_bindgen]
    pub fn draw_square(
        &self,
        x_position: f64,
        y_position: f64,
        w: f64,
        h: f64,
        text: &str
    ){
        /* log(&format!(
            "Call draw_square x_position: {}, y_position: {}, w: {}, h: {}, text: {}, text_x: {}, text_y: {}",
            x_position,
            y_position,
            w,
            h,
            text,
            (x_position + (w/2.0)),
            (y_position + (h/2.0))
        )); */
        self.ctx.stroke_rect(
            x_position,
            y_position,
            w,
            h
        );

        self.text_in_rec(
            text,
            x_position + (w/2.0),
            y_position + (h/2.0)
        );
    }

    #[wasm_bindgen]
    pub fn draw_square_filled(
        &self,
        x_position: f64,
        y_position: f64,
        w: f64,
        h: f64,
        text: &str,
        kind: &str
    ){
        /*log(&format!(
            "Call draw_square_filled x_position: {}, y_position: {}, w: {}, h: {}, text: {}, text_x: {}, text_y: {}",
            x_position,
            y_position,
            w,
            h,
            text,
            (x_position + (w/2.0)),
            (y_position + (h/2.0))
        ));*/
        match kind {
            "M" => {
                self.ctx.set_fill_style(&JsValue::from_str("brown"));
            },
            "B" =>{
                self.ctx.set_fill_style(&JsValue::from_str("grey"));
            },
            "BE" =>{
                self.ctx.set_fill_style(&JsValue::from_str("SlateGray"));
            },
            _ => {
                self.ctx.set_fill_style(&JsValue::from_str("white"));        
            },
        }      

        //log("Grey color set to fill.");

        self.ctx.fill_rect(
            x_position,
            y_position,
            w,
            h
        );

        //log("Drawed rec succefully.");
        self.text_in_rec(
            text,
            x_position + (w/2.0),
            y_position + (h/2.0)
        );
    }    

    #[wasm_bindgen]
    pub fn log(&self, s: &str){
        log(s);
    }

    #[wasm_bindgen]
    pub fn text_in_rec(&self, text: &str, x:f64, y:f64){

        self.ctx.set_fill_style(&JsValue::from_str("black"));
        
        let text_result = self.ctx.fill_text(
            text,
            x,
            y
        );

        match text_result {
            Ok(_) => {/*log("Text in rec succefully.");*/}
            Err(e) => {log(&JsValue::as_string(&e).unwrap());}
        }
    }
}