use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use wasm_bindgen_futures::JsFuture;
use web_sys::{Request, RequestInit, RequestMode, Response};
use std::env;

#[wasm_bindgen]
extern {
    #[wasm_bindgen(js_namespace = console)]
    fn log(s: &str);
}

#[wasm_bindgen]
pub async fn create() -> Result<JsValue, JsValue> {

    let url: String = match env::var_os("SERVICE_URL") {
        Some(v) => v.into_string().unwrap(),
        None => "http://localhost".to_owned(),
    };

    log(format!("Request url: {}", url).as_str());

    let port: String = match env::var_os("PORT") {
        Some(v) => v.into_string().unwrap(),
        None => ":40040".to_owned(),
    };

    log(format!("Request port: {}", port).as_str());
    
	let mut opts = RequestInit::new();
    opts.method("GET");
    opts.mode(RequestMode::Cors);

    let url = format!("{}{}/{}", url, port,"map");

    log(format!("Final url: {}", url).as_str());

    let request = Request::new_with_str_and_init(&url, &opts)?;

    request
        .headers()
        .set("Accept", "application/json")?;

    let window = web_sys::window().unwrap();
    let resp_value = JsFuture::from(window.fetch_with_request(&request)).await?;

    // `resp_value` is a `Response` object.
    assert!(resp_value.is_instance_of::<Response>());
    let resp: Response = resp_value.dyn_into().unwrap();

    // Convert this other `Promise` into a rust `Future`.
    let json = JsFuture::from(resp.json()?).await?;
    return  Ok(json);
}