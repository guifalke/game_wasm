use serde::{Serialize, Deserialize};
use std::collections::HashMap;

#[derive(Deserialize, Serialize, Debug, Default)]
pub struct MapRoom {
    pub z_index: i32,
    pub x_start: i32,
    pub x_width: i32,
    pub y_start: i32,
    pub y_height: i32,
    pub kind: String,
}

#[derive(Deserialize, Serialize , Debug, Default)]
pub struct MapInstance {
    pub map_room: HashMap<String, MapRoom>,
    pub x_size: i32,
    pub y_size: i32,
}

impl MapRoom {

	pub fn get_end_x(&self) -> i32 {
		return self.x_start + self.x_width -1;
	}

	pub fn get_end_y(&self) -> i32 {
		return self.y_start + self.y_height -1;
	}

	pub fn is_end_of_room(&self, y_map_index: i32, x_map_index: i32) -> bool {
		return self.get_end_x() == x_map_index && self.get_end_y() == y_map_index;
	}

	pub fn is_inside_room(&self, y_map_index: i32, x_map_index: i32) -> bool {
		let test_x: bool = self.x_start <= x_map_index && self.get_end_x() >= x_map_index;
		let test_y: bool = self.y_start <= y_map_index && self.get_end_y() >= y_map_index;
		return test_x && test_y;
	}	
}

#[cfg(test)]
mod tests {
    use super::MapRoom;

    #[test]
    fn test_is_inside_room() {
        let m: MapRoom = MapRoom{
			z_index: 1,
            x_start: 14,
            x_width: 3,
            y_start: 0,
            y_height: 2,
            kind: "BS".to_owned()
		};

		assert_eq!(m.x_start <= 15, true);
		assert_eq!(m.get_end_x() > 15, true);
		assert_eq!(m.y_start <= 0, true);
		assert_eq!(m.get_end_y() > 0, true);
		assert_eq!(m.is_inside_room(0, 15), true);

		assert_eq!(m.x_start <= 16, true);
		assert_eq!(m.get_end_x() >= 16, true);
		assert_eq!(m.y_start <= 0, true);
		assert_eq!(m.get_end_y() >= 0, true);
		assert_eq!(m.is_inside_room(0, 16), true);

		assert_eq!(m.x_start <= 14, true);
		assert_eq!(m.get_end_x() >= 14, true);
		assert_eq!(m.y_start <= 1, true);
		assert_eq!(m.get_end_y() >= 1, true);
		assert_eq!(m.is_inside_room(1, 14), true);
    }
}