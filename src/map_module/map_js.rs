use wasm_bindgen::prelude::*;

use crate::map_module::map_client::create;
use crate::map_module::model::MapInstance;
use crate::map_module::map_implementation::MapUser;
use crate::map_module::map_draw_js::DrawWrapper;

use web_sys::CanvasRenderingContext2d;

#[wasm_bindgen]
extern {
    #[wasm_bindgen(js_namespace = console)]
    fn log(s: &str);
}

#[wasm_bindgen]
pub async fn draw(ctx: CanvasRenderingContext2d, number_room: i32) -> Result<(), JsValue>  {
    log("Start map draw.");
    log("Request map.");
    let response_json = create().await?;
    log("Response recieved json.");
    let response: MapInstance = serde_wasm_bindgen::from_value(response_json).unwrap();
    log("Response parsed into struct.");
    let mut map_user_instance: MapUser = MapUser::new(
        response,
        800,
        600,
        DrawWrapper::new(ctx)
    ).await;
    log("Map instance created.");
    map_user_instance.draw_mapuser(number_room);
    return Ok(());
}